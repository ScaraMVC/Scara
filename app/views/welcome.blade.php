<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Scara</title>
        <style type="text/css">
            @import url('https://fonts.googleapis.com/css?family=Magra');
            @import url('https://fonts.googleapis.com/css?family=Lato:100');

            body, html {
                font-family: Magra;
                height: 100vh;
                background-color: rgba(254, 154, 154, 0.07);
                margin: 0;
            }

            ::selection
            {
                background-color: rgba(254, 154, 154, 1);
                color: #EDE9E8;
            }

            ::-moz-selection
            {
                background-color: rgba(254, 154, 154, 1);
                color: #EDE9E8;
            }

            .window {
                display: none;
            }

            .container {
                align-items: center;
                display: flex;
                justify-content: center;
                height: 100vh;
            }

            .container .title {
                color: #999;
                width: auto;
            }

            .container .title > h1 {
                font-family: Lato;
                font-size: 10.2em;
                font-weight: lighter;
                padding: 0;
                margin: 0;
                text-align: center;
            }

            .container .title > p {
                padding: 0;
                margin-top: -15px;
            }

            .container .title > p.st {
                padding-bottom: 5px;
                margin-left: 38px;
            }

            .container .title > p.bor {
                border-bottom: 1px solid #aaa;
            }

            .container .title > p.st > .subtitle,
            .container .title > p.st > .bull {
                display: none;
            }

            .container .title > p.bm {
                padding-top: 5px;
                text-align: center;
            }

            .container .title > p > .subtitle {
                padding: 0 10px;
                font-size: 1.3em;
            }
        </style>
        <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/welcome.js') }}" type="text/javascript"></script>
    </head>
    <body>
        <div class="window">
            <div class="container">
                <div class="title">
                    <h1>Scara</h1>
                    <p class="st">
                        <span class="subtitle">Simple</span>
                        <span class="bull">&bull;</span>
                        <span class="subtitle">Lightweight</span>
                        <span class="bull">&bull;</span>
                        <span class="subtitle">Rapid</span>
                    </p>
                    <p class="bor"></p>
                    <p class="bm">
                        <span class="subtitle">Benchmark</span>
                    </p>
                    <p class="bm">
                        Load Time: {{ Benchmark::check('scara') }} ms
                        |
                        Memory Size: {{ Benchmark::memcheck() }} MB
                    </p>
                    <p class="bm">
                        Version: {{ version() }}
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
